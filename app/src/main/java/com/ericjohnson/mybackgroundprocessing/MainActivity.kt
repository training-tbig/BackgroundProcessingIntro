package com.ericjohnson.mybackgroundprocessing

import android.os.AsyncTask
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.ericjohnson.mybackgroundprocessing.R.drawable

import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.imgPicture

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        executeFromThread() 
        ExecuteFromAsyncTask().execute()
        executeFromRX()
    }

    private fun executeFromThread() {

        // Create a new thread
        Thread(Runnable {
            // Make this thread pause for 3 seconds
            Thread.sleep(3000)

            //  Set an Image from Resource Drawable
            val image = R.drawable.disco

            //set image to UI Thread with a Runnable
            imgPicture.post {
                imgPicture.setImageResource(image)
            }

        }).start() //Start this thread
    }


    inner class ExecuteFromAsyncTask : AsyncTask<Void, Int, Int>() {

        // Do something before execute a process in the thread asynchronously
        // in this case, show a toast (in Main Thread)
        override fun onPreExecute() {
            super.onPreExecute()
            Toast.makeText(baseContext,"Pre-execute",Toast.LENGTH_SHORT).show()
        }

        // Show asynchronous progress
        override fun onProgressUpdate(vararg values: Int?) {
            super.onProgressUpdate(*values)
        }

        //Do the process asynchronously (in Worker Thread)
        override fun doInBackground(vararg param: Void?): Int? {
            Thread.sleep(3000)
            return R.drawable.disco
        }

        // Show the result of asynchronous task to the Main Thread
        override fun onPostExecute(result: Int) {
            super.onPostExecute(result)
            imgPicture.setImageResource(result)
            Toast.makeText(baseContext,"Finish",Toast.LENGTH_SHORT).show()
        }
    }

    private fun executeFromRX() {

        //Create observer, the one who observe and react immediately when observable emits data
        val observer: Observer<Int> = object : Observer<Int> {
            override fun onComplete() {
            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(image: Int) {
                imgPicture.setImageResource(image)
            }

            override fun onError(e: Throwable) {
                Toast.makeText(baseContext, e.localizedMessage.toString(),
                    Toast.LENGTH_SHORT).show()
            }
        }

        //Create the observable, the one who emits the data
        Observable.create<Int> {
            Thread.sleep(3000)
            it.onNext(R.drawable.disco)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()) //make the changes happens on Main Thread
            .subscribe(observer) //connect observer and observable
    }

}
